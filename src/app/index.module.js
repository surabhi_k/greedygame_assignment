(function() {
  'use strict';

  angular
    .module('assignmentGreedygame', ['ngResource', 'ui.router', 'ui.bootstrap', 'toastr', 'ngLodash', 'ngAudio']);

})();
