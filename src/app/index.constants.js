/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
