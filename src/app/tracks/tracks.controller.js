(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .controller('TracksController', TracksController);

  /** @ngInject */
  function TracksController($scope, lodash, TracksService, GenresService, ngAudio) {
    var _ = lodash;      

    //variables
    $scope.currentId;
    $scope.addTrackDisplay = false;
    $scope.newTrackGenre; 
    $scope.currentTrackPlaying;
    $scope.currentTrackIndex;

    //methods
    $scope.toggleEditTrack= function(id) {      
      if($scope.currentId === id) {
        $scope.currentId = '';
      } else {
        $scope.currentId = id;
      }
    }

    $scope.showEditTrack = function(checkId) {
      return $scope.currentId === checkId;
    };

    $scope.update = function(trackId) {
      var object = {
        id: trackId,
        title: angular.element('#'+trackId).val()
      };
      
      $scope.editTrack = TracksService.editTrack(object)
      .$promise.then(function(data) {
        $scope.getTracksList();
        console.log("success");
      });
      $scope.toggleEditTrack(trackId);
    }

    $scope.toggleAddTrack = function() {
      $scope.addTrackDisplay = !($scope.addTrackDisplay);
      $scope.getGenresList();
    }

    $scope.addNewTrack = function() {
      
      var requestGenre =[];
      requestGenre.push($scope.newTrackGenre.id);
      
      var object = {
        title: angular.element('#newTrackTitle').val(),
        rating: angular.element('#newTrackRating').val(),
        genres: requestGenre
      };
      TracksService.createTrack(object)
      .$promise.then(function(data) {
        $scope.getTracksList();
        $scope.toggleAddTrack();
        console.log("success");
       });
    }

    $scope.getTracksList = function(page) { 

      TracksService.tracksList({
        page: page || 1
      })
      .$promise.then(function(data) {              
        $scope.prevUrl = data.previous ? data.previous.split("=")[1] || 1 : null;
        $scope.nextUrl = data.next ? data.next.split("=")[1] : null;      
        $scope.tracks = data.results;    
        for(var i = 0; i < $scope.tracks.length; i++) {
          $scope.tracks[i].genres =  _.map($scope.tracks[i].genres, 'name');        
        }       
     });
    }

    $scope.getGenresList = function(page) { 
      GenresService.genresList({
        page: page || 1
      })
      .$promise.then(function(data) {          
        $scope.prevUrl = data.previous ? data.previous.split("=")[1] || 1: null;
        $scope.nextUrl = data.next ? data.next.split("=")[1] : null;      
        $scope.genres = data.results;  
      });
    }

    $scope.playTrack = function(title, index) {
        var clientId = '708abc176b8af0125b78392b9f132b4d';

         SC.initialize({
          client_id: clientId    
        });
        
        // search track based on title and play
        SC.get('/tracks', {
          q: title
        }).then(function(tracks) {
          $scope.currentTrackIndex = index;
          $scope.currentTrackPlaying = tracks[0].permalink_url;
          $scope.trackImage = tracks[0].artwork_url;
          $scope.audio = ngAudio.play(tracks[0].stream_url + '?client_id=' + clientId );              
          console.log(tracks);
        });
    }

    $scope.showPlayer = function(checkId) {
      return $scope.currentTrackIndex === checkId ;
    };
    
    $scope.getTracksList();
    
  }
})();
