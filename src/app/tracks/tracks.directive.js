angular.module('assignmentGreedygame')
  .directive('soundCloudPlayer', function () {
    return {
      restrict: 'E',
      template: '<iframe width="100%" height="465" scrolling="no" frameborder="no"></iframe>',
      link: function (scope, element, attrs) {
        var iframe = element.find('iframe');
        var settings = [
          'buying=false',
          'liking=false',
          'download=false',
          'sharing=false',
          'show_artwork=true',
          'show_comments=false',
          'show_playcount=false',
          'show_user=true',
          'hide_related=true',
          'visual=false',
          'start_track=0',
          'callback=true',
          'auto_play=true'
        ]
        var url = attrs.url + '?' + settings.join('&');
        iframe.attr('src', 'https://w.soundcloud.com/player/?url=' + url);
        SC.Widget(iframe.get(0));
      }
    };
  });