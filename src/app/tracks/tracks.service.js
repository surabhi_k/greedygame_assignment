(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .service('TracksService', TracksService);

  function TracksService($resource) {

    var actions = {
      tracksList: {
        method: 'GET',
        url: 'http://104.197.128.152:8000/v1/tracks?page=:page',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      },

      editTrack: {
        method: 'POST',
        url: 'http://104.197.128.152:8000/v1/tracks/:id',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      },

      createTrack: {
        method: 'POST',
        url: 'http://104.197.128.152:8000/v1/tracks',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      }
    }

    var resource = $resource('', {id: '@id'}, actions);

    this.tracksList = function(params) {
      return resource.tracksList(params);
    }

    this.editTrack = function(params) {
      return resource.editTrack(params);
    }

    this.createTrack = function(params) {
      return resource.createTrack(params);
    }
  }
})();