(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .service('GenresService', GenresService);

  function GenresService($resource) {

    var actions = {
      genresList: {
        method: 'GET',
        url: 'http://104.197.128.152:8000/v1/genres?page=:page',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      },

      editGenre: {
        method: 'POST',
        url: 'http://104.197.128.152:8000/v1/genres/:id',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      },

      createGenre: {
        method: 'POST',
        url: 'http://104.197.128.152:8000/v1/genres',
        transformRequest: function(data, headers) {
          headers = angular.extend({}, headers, {
            'Content-Type': 'application/json'
          });
          return angular.toJson(data);
        }
      }
    }

    var resource = $resource('', {id: '@id'}, actions);

    this.genresList = function(params) {
      return resource.genresList(params);
    }

    this.editGenre = function(params) {
      return resource.editGenre(params);
    }

    this.createGenre = function(params) {
      return resource.createGenre(params);
    }
  }
})();