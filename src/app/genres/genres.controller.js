(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .controller('GenresController', GenresController);

  /** @ngInject */
  function GenresController($scope, GenresService, $rootScope) {      

    //variables
    $scope.currentId;
    $scope.addGenreDisplay = false;

    //methods
    $scope.toggleEditGenre = function(id) {      
      if($scope.currentId === id) {
        $scope.currentId = '';
      } else {
        $scope.currentId = id;
      }
    }

    $scope.showEditGenre = function(checkId) {
      return $scope.currentId === checkId;
    };

    $scope.update = function(genreId) {
      var object = {
        id: genreId,
        name: angular.element('#'+genreId).val()
      };
      
      $scope.editGenre = GenresService.editGenre(object)
      .$promise.then(function(data) {
        $scope.getGenresList();
        console.log("success");
      });
      $scope.toggleEditGenre(genreId);
    }

    $scope.toggleAddGenre = function() {
      $scope.addGenreDisplay = !($scope.addGenreDisplay);
    }

    $scope.addNewGenre = function() {
      var object = {
        name: angular.element('#newGenre').val()
      };
      GenresService.createGenre(object)
      .$promise.then(function(data) {
        $scope.getGenresList();
        $scope.toggleAddGenre();
        console.log("success");
       });
    }

    $scope.getGenresList = function(page) { 
      GenresService.genresList({
        page: page || 1
      })
      .$promise.then(function(data) {          
        $scope.prevUrl = data.previous ? data.previous.split("=")[1] || 1: null;
        $scope.nextUrl = data.next ? data.next.split("=")[1] : null;      
        $scope.genres = data.results;  
     });
    }
    
    $scope.getGenresList();
    
  }
})();
