(function() {
  'use strict';

  angular
    .module('assignmentGreedygame')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('tracks', {
        url: '/tracks',
        templateUrl: 'app/tracks/tracks.html',
        controller: 'TracksController',
        controllerAs: 'tc'
      })
      .state('genres', {
        url: '/genres',
        templateUrl: 'app/genres/genres.html',
        controller: 'GenresController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/tracks');
    $locationProvider.html5Mode(true)
  }

})();
